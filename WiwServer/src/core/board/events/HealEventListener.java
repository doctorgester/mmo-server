package core.board.events;

public interface HealEventListener {
	public void onHeal(HealEventContext context);
}
