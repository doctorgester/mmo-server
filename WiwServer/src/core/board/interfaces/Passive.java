package core.board.interfaces;

public interface Passive {
	public void update();
}
